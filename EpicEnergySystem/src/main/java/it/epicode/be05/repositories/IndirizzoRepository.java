package it.epicode.be05.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.epicode.be05.model.Indirizzo;

public interface IndirizzoRepository extends JpaRepository<Indirizzo, Long> {

}

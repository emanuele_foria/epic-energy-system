package it.epicode.be05.repositories;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.StatoFattura;

public interface FatturaRepository extends JpaRepository<Fattura, Long> {
	
	Page <Fattura>  findByCliente (Cliente cliente, Pageable page);
	
	Page <Fattura>  findByStatoFattura (StatoFattura statoFattura, Pageable page);
	Page <Fattura>  findByData (Date data, Pageable page);
	Page <Fattura>  findByAnno (int anno, Pageable page);
	Page <Fattura>  findByImportoBetween (BigDecimal importomin,BigDecimal importomax, Pageable page);
	
	
}

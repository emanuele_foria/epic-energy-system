package it.epicode.be05.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.epicode.be05.model.Role;

@Repository
public interface RolesRepository extends JpaRepository<Role, Long> {

}

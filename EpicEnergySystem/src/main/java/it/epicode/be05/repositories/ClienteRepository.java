package it.epicode.be05.repositories;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.epicode.be05.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

	
	Page <Cliente> findAll (Pageable page);
	
	@Query("SELECT c FROM Cliente c ORDER BY c.fatturatoAnnuale")
	Page <Cliente> orderByFatturatoAnnuale (Pageable page);
	
	@Query("SELECT c FROM Cliente c ORDER BY c.dataInserimento")
	Page <Cliente> orderByDataDiInserimento (Pageable page);
	
	@Query("SELECT c FROM Cliente c ORDER BY c.dataUltimoContatto")
	Page <Cliente> orderByDataUltimoContatto (Pageable page);
	
	@Query("SELECT c FROM Cliente c ORDER BY c.indirizzoSedeLegale.comune.provincia.sigla")
	Page <Cliente> orderByProvinciaSedeLegale (Pageable page);
	
	
	
	Page <Cliente>  findByFatturatoAnnuale (long fatturato, Pageable page);
	
	Page <Cliente>  findByDataInserimento  (LocalDate date, Pageable page);
	Page <Cliente>  findByDataUltimoContatto (LocalDate date, Pageable page);
	
	Page <Cliente>  findByRagioneSocialeContainsIgnoreCase (String nome, Pageable page);
	
	
	
}

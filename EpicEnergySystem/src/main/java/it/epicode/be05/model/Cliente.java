package it.epicode.be05.model;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name="Cliente")
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Cliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	private String ragioneSociale;
	private String partitaIva;
	private String email;
	private LocalDate dataInserimento;
	private LocalDate dataUltimoContatto;
	private long fatturatoAnnuale;
	private String pec;
	private long telefono;
	
	@OneToOne
	private ContattoCliente contattoCliente;

	@OneToOne
	private Indirizzo indirizzoSedeLegale;
	@OneToOne
	private Indirizzo indirizzoSedeOperativa;
	
	
	private TipoCliente tipo;
	
	@OneToMany
	private List<Fattura> fattura;
}

package it.epicode.be05.model;

import lombok.Data;

@Data
public class LoginRequestModel {
	private String username;
	private String password;
}

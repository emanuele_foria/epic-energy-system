package it.epicode.be05.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name="Indirizzo")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Indirizzo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private String via;
	private String civico;
	private String località;
	private int cap;
	
	@ManyToOne
	private Comune comune; 

}

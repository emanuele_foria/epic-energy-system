package it.epicode.be05.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name="ContattoCliente")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContattoCliente {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private String nomeContatto;
	private String cognomeContatto;
	private String emailContatto;
	private long telefonoContatto;
}

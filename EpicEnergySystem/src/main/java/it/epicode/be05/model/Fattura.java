package it.epicode.be05.model;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name="Fattura")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Fattura {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	private int anno;
	private Date data;
	private BigDecimal importo;
	private int numero;
	
	@ManyToOne
	private Cliente cliente;

	@OneToOne
	private StatoFattura statoFattura;
}

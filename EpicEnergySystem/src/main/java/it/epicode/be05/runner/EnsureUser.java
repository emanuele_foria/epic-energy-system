package it.epicode.be05.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import it.epicode.be05.model.Role;
import it.epicode.be05.model.User;
import it.epicode.be05.repositories.RolesRepository;
import it.epicode.be05.services.UserService;

@Component
public class EnsureUser {

	private static final Logger log = LoggerFactory.getLogger(EnsureUser.class);

	@Autowired
	UserService service;

	@Autowired
	RolesRepository roles;

	public void ensureUser() {
		log.info("Start EnsureUserRunner");
		var users = service.readAll(Pageable.unpaged()); // tutti gli utenti SENZA paginazione - Pageable.unpaged()
		if (users.isEmpty()) {
			var admin = Role.builder().roleName("admin").build();
			roles.save(admin);
			log.info("Nessun utente in archivio... ");
			var user = User.builder() // creo un utente
					.email("emanueleforia@gmail.com").fullName("Emanuele Foria").username("manuforia").password("password").build();
			user.getRoles().add(admin);
			log.info("Creo l'utente {}", user);
			service.create(user); // lo passo al service per l'inserimento
			log.info("Creazione effettuata {}.", user);
		}
	}

}

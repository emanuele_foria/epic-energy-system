package it.epicode.be05.utils;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.epicode.be05.configuration.AppUserDetails;

@Component
public class JwtUtils { // centralizza la gestione del JWT
	private static final Logger log = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${jwt.secret}")
	private String securityKey;
	@Value("${jwt.expiration}")
	private long expirationMs;

	// generazione del token a partire dai dati di autenticazione
	public String generateToken(Authentication auth) {
		// recupero l'utente
		AppUserDetails user = (AppUserDetails) auth.getPrincipal();
		// imposto la data di scadenza <--- lo bypassiamo perché non diamo scadenza
		// creo il token
		var token = Jwts.builder() // builder per il token
				.setSubject(user.getUsername()) // contenuto del token
				.setIssuedAt(new Date()) // data di rilascio del token
				.setExpiration(new Date(new Date().getTime() + expirationMs)) // data di scadenza
				.signWith(SignatureAlgorithm.HS512, securityKey) // firma e crittografia del token
				.compact(); // trasforma il token in stringa
		return token;
	}

	// dato un token recupera il nome utente in esso memorizzato
	public String getUsernameFromToken(String token) {
		var username = Jwts.parser() // attiva l'analizzatore di token interno alla libreria JJWT
				.setSigningKey(securityKey) // chiave per la decodifica
				.parseClaimsJws(token) // analizza il contenuto del token
				.getBody() // prende il contenuto analizzato
				.getSubject(); // recupera quello che è contenuto nel subject
		return username;
	}

	// controlla se un token è valido
	public boolean isTokenValid(String token) {
		try {
			Jwts.parser() // attiva l'analizzatore di token interno alla libreria JJWT
			.setSigningKey(securityKey) // chiave per la decodifica
			.parseClaimsJws(token); // analizza il token (tenta di leggerlo)
			return true; // se non ci sono errori il token è valido
		} catch (Exception e) {
			log.error("Token non valido");
		}
		return false;
	}
}

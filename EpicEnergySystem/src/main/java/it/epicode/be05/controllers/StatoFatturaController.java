package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.services.StatoFatturaService;

@RestController
@RequestMapping("/api/statoFattura")
public class StatoFatturaController {

	@Autowired
	StatoFatturaService service;
	
	//@RolesAllowed("admin")
	@PostMapping("/save")
	public ResponseEntity<StatoFattura> saveStatoFattura(@RequestBody StatoFattura stato){
		service.create(stato);
		return new ResponseEntity<StatoFattura>(stato, HttpStatus.ACCEPTED);
	}
	
	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteStatoFattura (@PathVariable("id")long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<StatoFattura> searchStatoFattura (@PathVariable("id")long id ){
		return new ResponseEntity<StatoFattura>(service.readById(id), HttpStatus.OK);
	}
	
	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<StatoFattura> modifyStatoFattura (@RequestBody StatoFattura stato,@PathVariable("id") long id){
		service.update(id, stato);
		return new ResponseEntity<StatoFattura>(stato, HttpStatus.ACCEPTED);
	}
	
	
	
	
	
	
	
}

package it.epicode.be05.controllers;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.Cliente;
import it.epicode.be05.services.ClienteService;
import it.epicode.be05.services.ContattoClienteService;
import it.epicode.be05.services.IndirizzoService;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {
	@Autowired
	ClienteService service;
	@Autowired
	IndirizzoService serviceIndirizzo;
	@Autowired
	ContattoClienteService serviceContatto;

	//@RolesAllowed("admin")
	@PostMapping("/save")
	public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente){
		serviceIndirizzo.create(cliente.getIndirizzoSedeLegale());
		serviceIndirizzo.create(cliente.getIndirizzoSedeOperativa());
		serviceContatto.create(cliente.getContattoCliente());
		service.create(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.ACCEPTED);
	}
	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteCliente (@PathVariable("id")long id) {
		service.delete(id);		
	}

	@GetMapping("/search/{id}")
	public ResponseEntity<Cliente> searchCliente (@PathVariable("id") long id){
		return new ResponseEntity<Cliente>(service.readById(id),HttpStatus.ACCEPTED );
	}

	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<Cliente> modifyCliente (@RequestBody Cliente cliente,@PathVariable("id") long id){
		service.update(id, cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.ACCEPTED);
	}

	@GetMapping("/order/ragioneSociale")
	public ResponseEntity<Page<Cliente>>ordinaByRagioneSociale() throws Exception{
		try {
			Page<Cliente> result = service.findByRagioneSociale();
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}

	@GetMapping("/order/fatturatoAnnuale")
	public ResponseEntity<Page<Cliente>>ordinaByFatturatoAnnuale() throws Exception{
		try {
			Page<Cliente> result = service.orderByFatturatoAnnuale();
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}
	@GetMapping("/order/dataInserimento")
	public ResponseEntity<Page<Cliente>>ordinaByDataDiInserimento() throws Exception{
		try {
			Page<Cliente> result = service.orderByDataDiInserimento();
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}

	@GetMapping("/order/dataUltimoContatto")
	public ResponseEntity<Page<Cliente>>ordinaByDataUltimoContatto() throws Exception{
		try {
			Page<Cliente> result = service.orderByDataUltimoContatto();
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}

	@GetMapping("/order/provincia")
	public ResponseEntity<Page<Cliente>>ordinaByProvinciaSedeLegale() throws Exception{
		try {
			Page<Cliente> result = service.orderByProvinciaSedeLegale();
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}	

	@GetMapping("/filter/{FatturatoAnnuale}")
	public ResponseEntity<Page<Cliente>>filtraPerFatturatoAnnuale(@RequestParam long fatturato) throws Exception{
		try {
			Page<Cliente> result = service.findByFatturatoAnnuale(fatturato);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}	

	@GetMapping("/filter/{nome}")
	public ResponseEntity<Page<Cliente>>filtraPerParteNome(@RequestParam String nome) throws Exception{
		try {
			Page<Cliente> result = service.findByParteNome(nome);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}	


	@GetMapping("/filter/{dataInserimento}")
	public ResponseEntity<Page<Cliente>>filtraPerDataInserimento(@RequestParam LocalDate date) throws Exception{
		try {
			Page<Cliente> result = service.findByDataInserimento(date);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}	


	@GetMapping("/filter/{DataUltimoContatto}")
	public ResponseEntity<Page<Cliente>>filtraPerDataUltimoContatto(@RequestParam LocalDate date) throws Exception{
		try {
			Page<Cliente> result = service.findByDataUltimoContatto(date);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Cliente non trovato");
		}
	}	
}
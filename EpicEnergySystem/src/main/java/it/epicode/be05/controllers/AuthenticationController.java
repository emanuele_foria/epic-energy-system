package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.epicode.be05.model.LoginRequestModel;
import it.epicode.be05.model.LoginResponseModel;
import it.epicode.be05.utils.JwtUtils;

@RestController
@RequestMapping("/api/authentication")
public class AuthenticationController {

	// gestore dell'autenticazione
	@Autowired
	private AuthenticationManager auth;
	
	@Autowired
	JwtUtils jwt;
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginRequestModel login){
		// cerco l'utente tramite username e password
		var a = auth.authenticate(new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword()));
		// se lo trovo - cioè se non ho ottenuto alcun errore
		
		a.getAuthorities(); // forza la lettura dei ruoli
		// prima lo autentico nel sistema
		// simulo il login all'interno del contesto di sicurezza
		SecurityContextHolder.getContext().setAuthentication(a);
		
		// poi gli mando il token da conservare per i futuri accessi
		var token =jwt.generateToken(a); 
		
		return new ResponseEntity<>(LoginResponseModel.builder().token(token).build(), HttpStatus.OK);
	}
}

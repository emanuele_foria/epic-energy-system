package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.Role;
import it.epicode.be05.services.RoleService;

@RestController
@RequestMapping("/api/role")
public class RoleController {
	@Autowired
	RoleService service;
	
	//@RolesAllowed("admin")
	@PostMapping("/save")
	public ResponseEntity<Role> saveRole (@RequestBody Role role){
		service.create(role);
		return new ResponseEntity<Role>(role, HttpStatus.ACCEPTED);
	}

	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteRole (@PathVariable("id")long id) {
		service.delete(id);		
	}
	
	@GetMapping("/search/{id}")
	public ResponseEntity<Role> searchRole (@PathVariable("id")long id){
		return new ResponseEntity<Role>(service.readById(id),HttpStatus.ACCEPTED );
	}
	
	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<Role> modifyRole (@RequestBody Role role,@PathVariable("id") long id){
		service.update(id, role);
		return new ResponseEntity<Role>(role, HttpStatus.ACCEPTED);
	}
	
	
}

package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.ContattoCliente;
import it.epicode.be05.services.ContattoClienteService;

@RestController
@RequestMapping("/api/contattoCliente")
public class ContattoClienteController {

	@Autowired
	ContattoClienteService service;
	
	//@RolesAllowed("admin")
	@PostMapping("/save")
	public ResponseEntity<ContattoCliente> saveContattoCliente(@RequestBody ContattoCliente contatto){
		service.create(contatto);
		return new ResponseEntity<ContattoCliente>(contatto, HttpStatus.ACCEPTED);
	}
	
	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteContattoCliente (@PathVariable("id")long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<ContattoCliente> searchContattoCliente (@PathVariable("id")long id ){
		return new ResponseEntity<ContattoCliente>(service.readById(id), HttpStatus.OK);
	}
	
	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<ContattoCliente> modifyContattoCliente (@RequestBody ContattoCliente contatto,@PathVariable("id") long id){
		service.update(id, contatto);
		return new ResponseEntity<ContattoCliente>(contatto, HttpStatus.ACCEPTED);
	}
	
	
	
	
	
}

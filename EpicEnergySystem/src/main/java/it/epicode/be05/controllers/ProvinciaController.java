package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.Provincia;
import it.epicode.be05.services.ProvinciaService;

@RestController
@RequestMapping("/api/provincia")
public class ProvinciaController {

	@Autowired
	ProvinciaService service;
	
	//@RolesAllowed("admin")
	@PostMapping("/save")
	public ResponseEntity<Provincia> saveProvincia(@RequestBody Provincia provincia){
		service.create(provincia);
		return new ResponseEntity<Provincia>(provincia, HttpStatus.ACCEPTED);
	}
	
	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteProvincia (@PathVariable("id")long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<Provincia> searchProvincia (@PathVariable("id")long id ){
		return new ResponseEntity<Provincia>(service.readById(id), HttpStatus.OK);
	}
	
	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<Provincia> modifyProvincia (@RequestBody Provincia provincia,@PathVariable("id") long id){
		service.update(id, provincia);
		return new ResponseEntity<Provincia>(provincia, HttpStatus.ACCEPTED);
	}
	
	
	
}

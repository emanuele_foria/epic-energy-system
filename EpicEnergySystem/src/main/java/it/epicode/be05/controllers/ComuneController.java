package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.Comune;
import it.epicode.be05.runner.Anagrafica;
import it.epicode.be05.services.ComuneService;
import it.epicode.be05.services.ProvinciaService;

@RestController
@RequestMapping("/api/comune")
public class ComuneController {

	@Autowired
	ComuneService service;
	
	@Autowired
	Anagrafica anagrafica;
	
	@Autowired
	ProvinciaService serviceProvincia;
	
	//@RolesAllowed("admin")
	@PostMapping("/save")
	public ResponseEntity<Comune> saveComune(@RequestBody Comune comune){
		serviceProvincia.create(comune.getProvincia());
		service.create(comune);
		return new ResponseEntity<Comune>(comune, HttpStatus.ACCEPTED);
	}
	
	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteComune (@PathVariable("id")long id) {
		service.delete(id);
	}
	
	
	@GetMapping("/search/{id}")
	public ResponseEntity<Comune> searchComune (@PathVariable("id")long id ){
		return new ResponseEntity<Comune>(service.readById(id), HttpStatus.OK);
	}
	
	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<Comune> modifyComune (@RequestBody Comune comune,@PathVariable("id") long id){
		service.update(id, comune);
		return new ResponseEntity<Comune>(comune, HttpStatus.ACCEPTED);
	}
	
	//@RolesAllowed("admin")
	@PostMapping("/save/files")
	public ResponseEntity<?> readFile () {
		try{
			anagrafica.readProvincieComuni();
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		}catch(Exception e){
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}		
	}
		
}

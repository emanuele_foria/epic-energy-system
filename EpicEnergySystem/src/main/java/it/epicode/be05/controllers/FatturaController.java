package it.epicode.be05.controllers;

import java.math.BigDecimal;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.services.FatturaService;
import it.epicode.be05.services.StatoFatturaService;

@RestController
@RequestMapping("/api/fattura")
public class FatturaController {

	@Autowired
	FatturaService service;
	@Autowired
	StatoFatturaService serviseStato;


	//@RolesAllowed("admin")
	@PostMapping("/save")
	public ResponseEntity<Fattura> saveFattura(@RequestBody Fattura fattura){
		serviseStato.create(fattura.getStatoFattura());
		service.create(fattura);
		return new ResponseEntity<Fattura>(fattura, HttpStatus.ACCEPTED);
	}

	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteFattura (@PathVariable("id")long id) {
		service.delete(id);
	}


	@GetMapping("/search/{id}")
	public ResponseEntity<Fattura> searchFattura (@PathVariable("id")long id ){
		return new ResponseEntity<Fattura>(service.readById(id), HttpStatus.OK);
	}
	

	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<Fattura> modifyFattura (@RequestBody Fattura fattura,@PathVariable("id") long id){
		service.update(id, fattura);
		return new ResponseEntity<Fattura>(fattura, HttpStatus.ACCEPTED);
	}

	@GetMapping("/filter/{importo}")
	public ResponseEntity<Page<Fattura>>filtraPerImporto(@RequestParam BigDecimal importomin,BigDecimal importomax) throws Exception{
		try {
			Page<Fattura> result = service.findByRangeImporti(importomin, importomax);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Fattura non trovata");
		}
	}	

	@GetMapping("/filter/{anno}")
	public ResponseEntity<Page<Fattura>>filtraPerAnno(@RequestParam int anno) throws Exception{
		try {
			Page<Fattura> result = service.findByAnno(anno);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Fattura non trovata");
		}
	}

	@GetMapping("/filter/{data}")
	public ResponseEntity<Page<Fattura>>filtraPerData(@RequestParam Date data) throws Exception{
		try {
			Page<Fattura> result = service.findByData(data);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Fattura non trovata");
		}
	}

	@GetMapping("/filter/{statoFattura}")
	public ResponseEntity<Page<Fattura>>filtraPerStatoFattura(@RequestParam StatoFattura statoFattura) throws Exception{
		try {
			Page<Fattura> result = service.findByStatoFattura(statoFattura);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Fattura non trovata");
		}
	}

	@GetMapping("/filter/{cliente}")
	public ResponseEntity<Page<Fattura>>filtraPerCliente(@RequestParam Cliente cliente) throws Exception{
		try {
			Page<Fattura> result = service.findByCliente(cliente);
			if(result.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}else {
				return new ResponseEntity<>(result,HttpStatus.OK);
			}
		} catch (Exception e) {
			throw new Exception("Fattura non trovata");
		}
	}   

}
package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.epicode.be05.model.Indirizzo;
import it.epicode.be05.services.ComuneService;
import it.epicode.be05.services.IndirizzoService;
import it.epicode.be05.services.ProvinciaService;

@RestController
@RequestMapping("/api/indirizzo")
public class IndirizzoController {

    @Autowired
    IndirizzoService service;
    @Autowired
    ComuneService service1;
    @Autowired
    ProvinciaService service2;


  //@RolesAllowed("admin")
    @PostMapping("/save")
    public ResponseEntity<Indirizzo> saveIndirizzo(@RequestBody Indirizzo indirizzo){
    	service1.create(indirizzo.getComune());
        service.create(indirizzo);
        return new ResponseEntity<Indirizzo>(indirizzo, HttpStatus.ACCEPTED);
    }

  //@RolesAllowed("admin")
    @GetMapping("/delete/{id}")
    public void deleteIndirizzo (@PathVariable("id")long id) {
        service.delete(id);
    }


    @GetMapping("/search/{id}")
    public ResponseEntity<Indirizzo> searchIndirizzo (@PathVariable("id")long id ){
        return new ResponseEntity<Indirizzo>(service.readById(id), HttpStatus.OK);
    }

  //@RolesAllowed("admin")
    @PutMapping("/modify/{id}")
    public ResponseEntity<Indirizzo> modifyIndirizzo (@RequestBody Indirizzo indirizzo,@PathVariable("id") long id){
        service.update(id, indirizzo);
        return new ResponseEntity<Indirizzo>(indirizzo, HttpStatus.ACCEPTED);
    }

}
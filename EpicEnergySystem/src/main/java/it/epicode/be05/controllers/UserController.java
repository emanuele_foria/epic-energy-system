package it.epicode.be05.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import it.epicode.be05.model.User;
import it.epicode.be05.services.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService service;
	
	@PostMapping("/save")
	public ResponseEntity<User> saveUser (@RequestBody User user) {
		service.create(user);
		return new ResponseEntity<User>(user, HttpStatus.ACCEPTED);
		
	}
	
	//@RolesAllowed("admin")
	@GetMapping("/delete/{id}")
	public void deleteUser (@PathVariable("id")long id) {
		service.delete(id);		
	}
	
	@GetMapping("/search/{id}")
	public ResponseEntity<User> searchUser (@PathVariable("id")long id){
		return new ResponseEntity<User>(service.readById(id),HttpStatus.ACCEPTED );
	}
	
	//@RolesAllowed("admin")
	@PutMapping("/modify/{id}")
	public ResponseEntity<User> modifyUser (@RequestBody User user,@PathVariable("id") long id){
		service.update(id, user);
		return new ResponseEntity<User>(user, HttpStatus.ACCEPTED);
	}
	
	
	
}

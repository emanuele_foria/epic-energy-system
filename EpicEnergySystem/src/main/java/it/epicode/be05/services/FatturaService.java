package it.epicode.be05.services;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.data.domain.Page;
import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.StatoFattura;

public interface FatturaService extends CrudService<Fattura> {

	Page <Fattura>  findByCliente (Cliente cliente);
	Page <Fattura>  findByStatoFattura (StatoFattura statoFattura);
	Page <Fattura>  findByData (Date data);
	Page <Fattura>  findByAnno (int anno);
	Page <Fattura>  findByRangeImporti (BigDecimal importomin,BigDecimal importomax);
}

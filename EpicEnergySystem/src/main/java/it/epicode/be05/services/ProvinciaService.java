package it.epicode.be05.services;

import it.epicode.be05.model.Provincia;

public interface ProvinciaService extends CrudService<Provincia> {

	public Provincia findByNome (String nome);
}

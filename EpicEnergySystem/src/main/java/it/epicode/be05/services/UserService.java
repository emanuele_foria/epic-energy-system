package it.epicode.be05.services;

import it.epicode.be05.model.User;

public interface UserService extends CrudService<User> {

	User getCurrentUser();
}

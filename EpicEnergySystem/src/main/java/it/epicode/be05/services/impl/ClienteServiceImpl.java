package it.epicode.be05.services.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Comune;
import it.epicode.be05.repositories.ClienteRepository;
import it.epicode.be05.repositories.ComuneRepository;
import it.epicode.be05.services.ClienteService;
@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	ClienteRepository repo;
	
	@Autowired
	ComuneRepository crepository;
	
	@Override
	public void create(Cliente entity) {
//		if(entity!=null && entity.getIndirizzoSedeLegale()!=null) {
//            Comune comune=crepository.findById(entity.getIndirizzoSedeLegale().getComune().getId()).orElse(null);
//            entity.getIndirizzoSedeLegale().setComune(comune);
//            }
//            if(entity!=null && entity.getIndirizzoSedeOperativa()!=null) {
//                Comune comune2=crepository.findById(entity.getIndirizzoSedeOperativa().getComune().getId()).orElse(null);
//                entity.getIndirizzoSedeOperativa().setComune(comune2);
//                }
		repo.save(entity);
	}

	@Override
	public Cliente readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, Cliente entity) {
	var p= repo.findById(id).orElseThrow();
	p.setContattoCliente(entity.getContattoCliente());
	p.setDataInserimento(entity.getDataInserimento());
	p.setDataUltimoContatto(entity.getDataUltimoContatto());
	p.setEmail(entity.getEmail());
	p.setFattura(entity.getFattura());
	p.setFatturatoAnnuale(entity.getFatturatoAnnuale());
	p.setIndirizzoSedeLegale(entity.getIndirizzoSedeLegale());
	p.setIndirizzoSedeOperativa(entity.getIndirizzoSedeOperativa());
	p.setPartitaIva(entity.getPartitaIva());
	p.setPec(entity.getPec());
	p.setRagioneSociale(entity.getRagioneSociale());
	p.setTelefono(entity.getTelefono());
	p.setTipo(entity.getTipo());
	repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}

	@Override
	public Page<Cliente> readAll(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	public Page<Cliente> orderByFatturatoAnnuale() {
		Pageable page = PageRequest.of(0, 8);
		return repo.orderByFatturatoAnnuale(page);
	}

	@Override
	public Page<Cliente> orderByDataDiInserimento() {
		Pageable page = PageRequest.of(0, 8);
		return repo.orderByDataDiInserimento(page);
	}

	@Override
	public Page<Cliente> orderByDataUltimoContatto() {
		Pageable page = PageRequest.of(0, 8);
		return repo.orderByDataUltimoContatto(page);
	}

	@Override
	public Page<Cliente> orderByProvinciaSedeLegale() {
		Pageable page = PageRequest.of(0, 8);
		return repo.orderByProvinciaSedeLegale(page);
	}

	@Override
	public Page<Cliente> findByFatturatoAnnuale(long fatturato) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByFatturatoAnnuale(fatturato, page);
	}

	@Override
	public Page<Cliente> findByDataInserimento(LocalDate date) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByDataInserimento(date, page);
	}

	@Override
	public Page<Cliente> findByDataUltimoContatto(LocalDate date) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByDataUltimoContatto(date, page);
	}

	@Override
	public Page<Cliente> findByParteNome(String nome) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByRagioneSocialeContainsIgnoreCase(nome, page);
	}

	@Override
	public Page<Cliente> findByRagioneSociale() {
		Pageable page = PageRequest.of(0, 8, Sort.by("ragioneSociale"));
		return repo.findAll(page);
	}

}

package it.epicode.be05.services;


import it.epicode.be05.model.Indirizzo;

public interface IndirizzoService extends CrudService<Indirizzo> {

}

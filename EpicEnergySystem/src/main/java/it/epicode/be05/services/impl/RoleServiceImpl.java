package it.epicode.be05.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Role;
import it.epicode.be05.repositories.RolesRepository;
import it.epicode.be05.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService{
	@Autowired
	RolesRepository repo;
	
	@Override
	public void create(Role entity) {
		repo.save(entity);
		
	}

	@Override
	public Role readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public Page<Role> readAll(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	public void update(long id, Role entity) {
		var role = repo.findById(id).orElseThrow();
		role.setRoleName(entity.getRoleName());
		repo.save(role);
		
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
		
	}

}

package it.epicode.be05.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.ContattoCliente;
import it.epicode.be05.repositories.ContattoClienteRepository;
import it.epicode.be05.services.ContattoClienteService;
@Service
public class ContattoClienteServiceImpl implements ContattoClienteService{
	
	@Autowired
	ContattoClienteRepository repo;

	@Override
	public void create(ContattoCliente entity) {
		repo.save(entity);
	}

	@Override
	public ContattoCliente readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, ContattoCliente entity) {
		var p= repo.findById(id).orElseThrow();
		p.setNomeContatto(entity.getNomeContatto());
		p.setCognomeContatto(entity.getCognomeContatto());
		p.setEmailContatto(entity.getEmailContatto());
		p.setTelefonoContatto(entity.getTelefonoContatto());
		repo.save(p);		
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
		
	}

	@Override
	public Page<ContattoCliente> readAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}

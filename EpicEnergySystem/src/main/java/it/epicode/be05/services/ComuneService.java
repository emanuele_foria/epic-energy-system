package it.epicode.be05.services;

import it.epicode.be05.model.Comune;

public interface ComuneService extends CrudService<Comune> {

}

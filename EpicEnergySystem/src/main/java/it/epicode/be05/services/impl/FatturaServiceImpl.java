package it.epicode.be05.services.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.epicode.be05.model.Cliente;
import it.epicode.be05.model.Fattura;
import it.epicode.be05.model.StatoFattura;
import it.epicode.be05.repositories.FatturaRepository;
import it.epicode.be05.services.FatturaService;
@Service
public class FatturaServiceImpl implements FatturaService{
	
	@Autowired
	FatturaRepository repo;

	@Override
	public void create(Fattura entity) {
		repo.save(entity);
		
	}

	@Override
	public Fattura readById(long id) {
		return repo.findById(id).orElseThrow();
	}

	@Override
	public void update(long id, Fattura entity) {
		var p= repo.findById(id).orElseThrow();
		p.setAnno(entity.getAnno());
		p.setData(entity.getData());
		p.setImporto(entity.getImporto());
		p.setNumero(entity.getNumero());
		p.setStatoFattura(entity.getStatoFattura());
		repo.save(p);
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
	}


	@Override
	public Page<Fattura> findByCliente(Cliente cliente) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByCliente(cliente, page);
	}

	@Override
	public Page<Fattura> findByStatoFattura(StatoFattura statoFattura) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByStatoFattura(statoFattura, page);
	}

	@Override
	public Page<Fattura> findByData(Date data) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByData(data, page);
	}

	@Override
	public Page<Fattura> findByAnno(int anno) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByAnno(anno, page);
	}

	@Override
	public Page<Fattura> findByRangeImporti(BigDecimal importomin,BigDecimal importomax) {
		Pageable page = PageRequest.of(0, 8);
		return repo.findByImportoBetween(importomin,importomax, page);
	}

	@Override
	public Page<Fattura> readAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

}

package it.epicode.be05.services;
import java.time.LocalDate;
import org.springframework.data.domain.Page;
import it.epicode.be05.model.Cliente;

public interface ClienteService extends CrudService<Cliente> {

	
	Page <Cliente> orderByFatturatoAnnuale ();
	
	Page <Cliente> orderByDataDiInserimento ();
	
	Page <Cliente> orderByDataUltimoContatto ();
	
	Page <Cliente> orderByProvinciaSedeLegale ();
	
		
	
	Page <Cliente>  findByFatturatoAnnuale (long fatturato);
	
	Page <Cliente>  findByDataInserimento  (LocalDate date);
	Page <Cliente>  findByDataUltimoContatto (LocalDate date);
	
	Page <Cliente>  findByParteNome (String nome);
	Page <Cliente> findByRagioneSociale ();
}

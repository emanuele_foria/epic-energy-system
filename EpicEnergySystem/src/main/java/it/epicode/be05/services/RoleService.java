package it.epicode.be05.services;

import it.epicode.be05.model.Role;

public interface RoleService extends CrudService<Role>{

}

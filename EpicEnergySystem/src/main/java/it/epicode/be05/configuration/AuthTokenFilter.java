package it.epicode.be05.configuration;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import it.epicode.be05.utils.JwtUtils;

// gestisce la fase di una richiesta che abbia già ottenuto un token in precedenza
public class AuthTokenFilter extends OncePerRequestFilter {

	private static final Logger log = LoggerFactory.getLogger(AuthTokenFilter.class);

	// dichiaro la mia volontà di usare la classe di utilità per la gestione dei
	// token
	@Autowired
	JwtUtils jwt;

	// dichiaro la mia volontà di usare il servizio di sistema per la creazione
	// dell'utente di applicazione
	@Autowired
	AppUserDetailsService userDetailsService;

	private static final String AUTH_HEADER = "Authorization";
	private static final String TOKEN_BEARER = "Bearer";

	// questo metodo viene invocato dalla catena di gestione dell'autenticazione ad
	// ogni richiesta da parte del client
	// serve per autenticare un utente dalla lettura del token inviato al server
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		log.info("In AuthTokenFilter");
		try {
			// legge il token che si trova dentro allo header Authorization
			var header = request.getHeader(AUTH_HEADER);
			// recupera il valore del token senza la string Bearer
			if (header != null && header.length() > 0 && header.startsWith(TOKEN_BEARER)) {
				// tolgo la parte iniziale dallo header
				var token = header.substring(TOKEN_BEARER.length());
				// leggo lo username dal token
				var username = jwt.getUsernameFromToken(token);
				
				var details = userDetailsService.loadUserByUsername(username); // recupera l'utente applicativo tramite il servizio custom
				
				// adesso devo istruire il sistema, facendo finta che è avvenuta una autenticazione da parte dell'utente
				// il sistema ha bisogno di un proprio "biglietto di ingresso"
				//                                          |
				//                                          V
				var authentication = new UsernamePasswordAuthenticationToken(details, null, details.getAuthorities());
				// decoro l'oggetto con informazioni di sistema
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				// adesso devo comunica al sistema di sicurezza che questo utente è da considerarsi come autenticato
				// gestore del contesto di sicurezza
				//      |                          qui "simulo" l'avvenuta autenticazione
				//      |                                    |
				//      V                                    V
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} catch (Exception e) {
			log.error("Impossibile costruire l'utente di applicazione");
		}
		// alla fine mi assicuro che la catena dei filtri attivi venga correttamente continuata
		filterChain.doFilter(request, response);
	}

}

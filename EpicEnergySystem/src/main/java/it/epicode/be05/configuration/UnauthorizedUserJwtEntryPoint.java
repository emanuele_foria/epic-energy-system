package it.epicode.be05.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

// entry point di default per l'autenticazione
// in pratica comunica che l'utente NON E' autorizzato
// in quanto presuppone che sia stato autorizzato tramite il filtro creato in precedenza
@Component
public class UnauthorizedUserJwtEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Utente non autorizzato");
		
	}

}
